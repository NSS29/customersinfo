////////////////////////////////////////////////////////////////////////////////
package com.nss.customersinfo.web.security;
////////////////////////////////////////////////////////////////////////////////
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.access.AccessDeniedHandler;;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
public class CustomerAccessDeniedHandler implements AccessDeniedHandler {

    private String errorPage;

    public CustomerAccessDeniedHandler() {
    }

    public CustomerAccessDeniedHandler(String errorPage) {
        this.errorPage = errorPage;
    }

    public String getErrorPage() {
        return errorPage;
    }

    public void setErrorPage(String errorPage) {
        this.errorPage = errorPage;
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response,
            AccessDeniedException accessDeniedException)
            throws IOException, ServletException {
        request.getSession().setAttribute("message", " Sorry."
                +" You don't have privilages to view this page!!!");
        response.sendRedirect(errorPage);
    }
}
////////////////////////////////////////////////////////////////////////////////