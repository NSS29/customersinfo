////////////////////////////////////////////////////////////////////////////////
package com.nss.customersinfo.web.controller;
////////////////////////////////////////////////////////////////////////////////
import com.nss.customersinfo.dao.CustomerDAO;
import com.nss.customersinfo.model.Customer;
import com.nss.customersinfo.web.form.Message;
import com.nss.customersinfo.web.util.UrlUtil;
import java.io.*;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.context.MessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
////////////////////////////////////////////////////////////////////////////////
@Controller
public class CustomerController {

    @Autowired
    MessageSource messageSource;

    @Autowired
    private CustomerDAO customerDAO;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String list(Model uiModel) {
        List<Customer> customers = customerDAO.findAll();
        uiModel.addAttribute("customers", customers);
        return "customers/list";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String show(@PathVariable("id") Long id, Model uiModel) {
        Customer customer = customerDAO.findByID(id);
        uiModel.addAttribute("customer", customer);
        return "customers/show";
    }

    @RequestMapping(value = "/{id}", params = "form", method = RequestMethod.GET)
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("customer", customerDAO.findByID(id));
        return "customers/edit";
    }

    @RequestMapping(value = "/", params = "form", method = RequestMethod.GET)
    public String createForm(Model uiModel) {
        Customer customer = new Customer();
        uiModel.addAttribute("customer", customer);
        return "customers/edit";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String updateOrInsert(@ModelAttribute("customer") @Valid Customer customer,
            BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest,
            RedirectAttributes redirectAttributes, Locale locale) {
        if (bindingResult.hasErrors()) {
            uiModel.addAttribute("message", new Message("error",
                    messageSource.getMessage("contact_save_fail", new Object[]{}, locale)));
            uiModel.addAttribute("customer", customer);
            return "customers/edit";
        }

        uiModel.asMap().clear();
        redirectAttributes.addFlashAttribute("message", new Message("success",
                messageSource.getMessage("contact_save_success", new Object[]{}, locale)));

        if (customer.getId() == null) {
            customerDAO.insert(customer);
        } else {
            customerDAO.update(customer);
        }

        return "redirect:/"
                + UrlUtil.encodeUrlPathSegment(customer.getId().toString(),
                        httpServletRequest);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable("id") Long id, Model uiModel) {
        customerDAO.delete(customerDAO.findByID(id));
        return "redirect:/";
    }
//------------------------------------------------------------------------------ 

    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public String accesssDenied(Model uiModel) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            uiModel.addAttribute("username", userDetail.getUsername());
        }
        return "403";
    }

    @RequestMapping(value = "/accessdenied", method = RequestMethod.GET)
    public String loginerror(Model uiModel) {
        uiModel.addAttribute("error", "true");
        return "denied";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "logout";
    }
//------------------------------------------------------------------------------
}
////////////////////////////////////////////////////////////////////////////////
