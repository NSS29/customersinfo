////////////////////////////////////////////////////////////////////////////////
package com.nss.customersinfo.model;
////////////////////////////////////////////////////////////////////////////////
import java.io.*;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
////////////////////////////////////////////////////////////////////////////////
/**
 *
 * @author NSS
 */
@MappedSuperclass
public class BaseEntity implements Serializable {
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID")
    private Long id;
    
    public BaseEntity() {}
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }   
}
////////////////////////////////////////////////////////////////////////////////