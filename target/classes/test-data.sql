INSERT INTO CUSTOMER (last_name, first_name, birth_date, email)
VALUES ('Ho', 'Clarence', '1980-07-30', 'ho@gmail.com');
INSERT INTO CUSTOMER (last_name, first_name, birth_date, email)
VALUES ('Tiger', 'Scott', '1990-11-02', 'tiger@gmail.com');
INSERT INTO CUSTOMER (last_name, first_name, birth_date, email)
VALUES ('Smith', 'John', '1964-02-28', 'smith@gmail.com');

INSERT INTO users(user_name, password, enabled) VALUES ('NSS', 'nss22', true);
INSERT INTO users(user_name, password, enabled) VALUES ('User', 'user', true);

INSERT INTO user_roles (user_name, role) VALUES ('NSS', 'ROLE_USER');
INSERT INTO user_roles (user_name, role) VALUES ('NSS', 'ROLE_ADMIN');
INSERT INTO user_roles (user_name, role) VALUES ('User', 'ROLE_USER');